package assignment4;

/**
 *
 * @author Yusuf
 */
public class Assignment4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        java.util.Scanner in = new java.util.Scanner(System.in);
        System.out.println ("Enter the radius: "); 
        double r = in.nextDouble();
        
        double aCircle = (r*r) + 22/7;
        System.out.println ("The area of a circle is: " + aCircle);
        
        
    }
    
}